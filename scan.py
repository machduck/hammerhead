#!/usr/bin/env python

import gevent.monkey
gevent.monkey.patch_all()

import gevent
from gevent.queue import JoinableQueue as Queue

import requests
from requests import Session
from requests.adapters import HTTPAdapter

import logging
import csv
import time
import random

import traceback

from utils import Handler
from config import WORKERS

CSV_FILE = 'top.csv' # XXX

RANK=0x0
SITE=0x1

def headers():
	return {'User-Agent': ''}

def scan( handler, site ):
	logging.info( '[*] Scanning {}'.format( site ))

	session = Session()
	adapter = HTTPAdapter( max_retries=3 )
	session.mount( 'http', adapter )

	try:
		r = session.get('http://{}'.format(site),
						headers = headers(),
						timeout = 15,
						verify = False )
	except:
		logging.critical( '[!] {}\n{}'.format( site, traceback.format_exc() ))
		return

	try:
		handler.add_headers( site, r.headers )
	except:
		logging.error( '[!] {}\n{}'.format( site, traceback.format_exc() ))

def worker( q ):
	handler = Handler()
	while True:
		scan( handler, *q.get())
		q.task_done()

def generator():
	with open( CSV_FILE, 'rb') as fh:
		reader = csv.reader( fh )
		for row in reader:
			yield ( row[ SITE ], )

def log( filename = None ):
	logging.basicConfig(level=logging.INFO, filename = filename,
						format='[%(asctime)s %(levelname)s] %(message)s',
						datefmt='%m/%d/%Y %I:%M:%S' )
	requests_logger = logging.getLogger("requests")
	requests_logger.setLevel( logging.WARNING )

def workers( q ):
	return [ gevent.spawn( worker, q ) for x in xrange( WORKERS ) ]

def main():
	log('scan.log')
	q = Queue(WORKERS)
	w = workers( q )
	for index,host in enumerate(generator()):
		q.put( host )
	q.join()

if __name__ == "__main__":
	main()
