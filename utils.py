#!/usr/bin/env python

from db import Session
from models import Header, Base

import logging
import traceback

import warnings
import sqlalchemy
warnings.simplefilter('ignore', sqlalchemy.exc.SAWarning)

class Handler( object ):

	def __init__( self ):
		self.session = Session()

	def add_headers( self, site, headers ):
		inserts = []

		for name,value in headers.iteritems():
			inserts.append( Header( site=site.decode('utf8'),
									name=name.decode('utf8'),
									value=value.decode('utf8') ))

		self.session.add_all( inserts )

		self.session.commit()

	def get_names( self ):
		for header in self.session.query(Header.name).distinct().all():
			yield( header.name )

	def get_names_with_count( self ):
		for name in self.get_names():
			num = self.session.query(Header.name).filter_by(name=name).count()
			yield( name, num )

	def get_headers_by_name( self, name ):
		amount = self.session.query(Header).filter_by(name=name).count()
		progress = 0
		limit = 10000
		while( progress < amount ):
			for header in self.session.query(Header).filter_by(name=name).offset(progress).limit(limit).all():
				yield( header )
			progress += limit

	def delete( self ):
		for table in reversed( Base.metadata.sorted_tables ):
			self.session.execute( table.delete() )
		self.session.commit()

if __name__ == "__main__": # instead of UT
	handler = Handler()

	handler.delete()

	handler.add_headers( 'google.com', {'hi': 'wut', 'hello': 'wot', 'sup': 'wat' })
	handler.add_headers( 'www.google.com', {'hi': 'wut', 'hello': 'wot' })
	
	assert( [ name for name in handler.get_names() ] == [u'hi', u'hello', u'sup'] )

	assert( [ (name,count) for (name,count) in handler.get_names_with_count() ] == [(u'hi', 2), (u'hello', 2), (u'sup', 1)] )

	headers = [ header for header in handler.get_headers_by_name( 'sup' ) ]
	assert( len( headers ) == 1 )
	header = headers[0]
	assert( header.site == 'google.com' )
	assert( header.name == 'sup' )
	assert( header.value == 'wat' )

	handler.delete()
