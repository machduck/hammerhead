#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Base

from config import WORKERS

engine = create_engine( "mysql+pymysql://root:root@localhost/hammerhead?charset=utf8",
						pool_size = 0xF,
						max_overflow = 0 )
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
