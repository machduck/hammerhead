#!/usr/bin/env python

import argparse

from utils import Handler

def main():
	parser = argparse.ArgumentParser(description='u mad br0?')
	parser.add_argument('--mode', choices=['headers','values','delete'],
						help='mode of operation' )
	parser.add_argument('--header', help='header to lookup')
	parser.add_argument('--count', action="store_true", help='frequency analysis on headers')
	args = parser.parse_args()
	
	handler = Handler()

	if args.mode == 'headers':
		if args.count:
			for name,size in handler.get_names_with_count():
				print '[{}] {}'.format( size, name.encode('utf8') )
		else:
			for name in handler.get_names():
				print '[*] {}'.format( name.encode('utf8') )

	elif args.mode == 'values':
		if args.header is None:
			argparse.ArgumentParser.error( 'specify header via --header option' )
		for header in handler.get_headers_by_name( args.header ):
			print '[*] {} {}'.format( header.site.encode('utf8'), header.value.encode('utf8') )

	elif args.mode == 'delete':
		handler.delete()

if __name__ == "__main__":
	main()
