#!/usr/bin/env python

from sqlalchemy import Column, Integer, String, Unicode, Text
from sqlalchemy import ForeignKey, Index
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relation

Base = declarative_base()
 
class Header( Base ):
	__tablename__ = "headers"

	mysql_engine = 'InnoDB'
	mysql_charset = 'utf8'

	id = Column( Integer, primary_key=True )
	name = Column( Text, nullable=False, index=True )
	site = Column( Text, nullable=False )
	value = Column( Text, nullable=False )

	def __init__( self, site, name, value ):
		self.site = site
		self.name = name
		self.value = value

	def __repr__ ( self ):
		return "<Header('{}','{}','{}')>".format( 	self.site,
													self.name,
													self.value )
